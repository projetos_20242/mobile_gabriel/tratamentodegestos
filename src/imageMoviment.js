
import React, { useRef, useState, useEffect } from "react";
import { View, Dimensions, PanResponder, Button, Text, Image } from "react-native";

const ImageMoviment = () => {
  const [count, setCount] = useState(0);
  const [imageIndex, setImageIndex] = useState(0);
  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.25;
  const images = [
    require("../assets/Caveira.jpg"),
    require("../assets/CaveiraFina.jpg")
  ]
  

  const panResponder = useRef(
    PanResponder.create({

      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if(Math.abs(gestureState.dx)> gestureThreshold){
            setCount((prevCount)=> prevCount + 1);
            if(gestureState.dx > 0 ){
                setImageIndex((prevIndex) => 
                prevIndex === images.length - 1? 0: prevIndex + 1 )
            }
            else{
                setImageIndex((prevIndex)=> 
                prevIndex=== 0? images.length - 1 :prevIndex - 1)
            }
        }
        
      },
    })
  ).current;
  return (
    <View {...panResponder.panHandlers} style={{flex:1, alignItems: "center", justifyContent: "center"}}>
        <Image source={images[imageIndex]} style={{width : 200, height: 200}}/>
      <Text>Valor do contador: {count}</Text>
    </View>
  );
};

export default ImageMoviment;
